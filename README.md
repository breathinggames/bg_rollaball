# RollABall
A game for health co-created by commonners of [Breathing Games](http://www.breathinggames.net), and released under free/libre licence.

Developed on [Unity 3D](http://unity3d.com), to be used on a [Firefox](https://www.mozilla.org/en-US/firefox/products/) browser or [Android](https://www.android.com) phone, with a [breathing device](https://gitlab.com/breathinggames/bg/wikis/5-hardware).

**Read the [documentation](https://gitlab.com/breathinggames/bg_rollaball/wikis/pages).**


## In short
Breath appropriately to have the ball grow and catch the objects more easily.


## Contributors
Contributors are welcome. To join, contact info (at) breathinggames.net